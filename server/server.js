const GRE = require("./getrunningexps.js");
const CTSS = require("./calcexpsessions");
const CW = require("./calcwinners");
const express = require("express"),
  http = require("http"),
  fs = require("fs"),
  path = require("path"),
  Datastore = require("nedb"),
  socket = require("socket.io");
const app = express();
const database = new Datastore({
  filename: "Database/datafile.db",
  autoload: true,
});
var orgData = [];

app.set("port", process.env.PORT || 8000);

app.use(express.static(path.join(__dirname, "..", "build")));

app.use(express.json({ limit: "1mb" }));

var server = http.createServer(app);

server.listen(app.get("port"), function () {
  console.log("Express server listening on port " + app.get("port"));
});

app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "..", "build", "index.html"));
});

const io = socket(server);

io.on("connection", (socket) => {
  socket.on("room-id", (data) => {
    if (data === "create") {
      io.to(socket.id).emit("room-ID", socket.id);
    }
    if (data.opt === "join") {
      socket.join(data.roomID);
    }
  });

  socket.on("startmessages", (data) => {
    var socketID = socket.id;
    io.to(socketID).emit("initmessage", data);
  });

  socket.on("send-accounts", (data) => {
    if (data) {
      var socketID = socket.id;

      orgData = GRE.getExps(data);
      orgData[orgData.length - 1] = CTSS.calcTSess(orgData[orgData.length - 1]);
      orgData[orgData.length - 1] = CW.calcWinner(orgData[orgData.length - 1]);

      io.to(socketID).emit("goto", "/exp-results");

      async function waitHere() {
        await new Promise((resolve, err) => {
          setTimeout(function () {
            resolve("Done!");
          }, 50);
        });
        io.to(socketID).emit("results", orgData[orgData.length - 1]);
      }
      waitHere();
    } else {
    }
  });
});
