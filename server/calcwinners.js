exports.calcWinner = function calculateWinnerVariant(AWRE) {
  AWRE.forEach((account) =>
    account.PWRE.forEach((property) =>
      property.PWREExps.forEach((experiment) => {
        experiment.experimentVariants.forEach((variant) => {
          if ("result" in variant) {
            variant.leadConversions = calcConversions(
              variant.result[0],
              variant.result[1]
            );
            variant.reservationConversions = calcConversions(
              variant.result[0],
              variant.result[3]
            );
          }
        });
        experiment.experimentVariants = calcIncrease(
          experiment.experimentVariants
        );
      })
    )
  );
  return AWRE;
};

function calcConversions(sessions, conversionRate) {
  return Math.round((Number(sessions) * Number(conversionRate)) / 100);
}

function calcIncrease(variants) {
  var originalLeadsCR = 0;
  var originalReservationsCR = 0;

  variants[0].leadsIncrease = 0;
  variants[0].reservationIncrease = 0;
  variants[0].TIncrease = 0;

  if ("result" in variants[0]) {
    originalLeadsCR = Number(variants[0].result[1]);
    originalReservationsCR = Number(variants[0].result[3]);
  }
  for (var i = 1; i < variants.length; i++) {
    if ("result" in variants[i]) {
      var variantLeadsCR = Number(variants[i].result[1]);
      var variantReservationsCR = Number(variants[i].result[3]);

      variants[i].leadsIncrease =
        ((variantLeadsCR - originalLeadsCR) /
          (originalLeadsCR != 0 ? originalLeadsCR : 1)) *
        100;

      variants[i].reservationIncrease =
        ((variantReservationsCR - originalReservationsCR) /
          (originalReservationsCR != 0 ? originalReservationsCR : 1)) *
        100;

      variants[i].TIncrease =
        variants[i].leadsIncrease + variants[i].reservationIncrease;
    } else {
      variants[i].leadsIncrease = 0;
      variants[i].reservationIncrease = 0;
      variants[i].TIncrease = 0;
    }
  }

  return variants;
}
