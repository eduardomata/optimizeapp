exports.calcTSess = function calculateTotalSessions(AWRE) {
  AWRE.forEach((account) =>
    account.PWRE.forEach((property) =>
      property.PWREExps.forEach((experiment) => {
        var totalSessions = [];
        experiment.experimentVariants.forEach((variant) => {
          if ("result" in variant) {
            totalSessions.push(variant.result[0]);
          }
        });

        if (totalSessions.length) {
          experiment.experimentTSessions = totalSessions.reduce(
            (acum, elem) => Number(acum) + Number(elem)
          );
        } else {
          experiment.experimentTSessions = 0;
        }
      })
    )
  );
  return AWRE;
};
