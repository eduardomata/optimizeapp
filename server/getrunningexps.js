exports.getExps = function getRunningExperiments(accounts) {
  var accountsPWRE = [];

  accounts.forEach((account) => {
    var PWRE = handleAccount(account);
    if (PWRE.length) {
      accountsPWRE.push({
        accountName: account.accountName,
        PWRE: PWRE,
      });
    }
  });

  accounts.push(accountsPWRE);
  return accounts;

  function handleAccount(account) {
    var propertiesWithRunningExps = [];
    account.accountProperties.forEach((property) => {
      var runningExperiments = handleProperty(property);
      if (runningExperiments.length) {
        propertiesWithRunningExps.push({
          PWREName: property.propertyName,
          PWREExps: runningExperiments,
        });
      }
    });
    return propertiesWithRunningExps;
  }

  function handleProperty(property) {
    var expArray = [];
    if (property.propertyExperiments.length) {
      property.propertyExperiments.forEach((experiment) => {
        var expResponse = handleExperiment(experiment);
        if (expResponse) {
          expArray.push(expResponse);
        }
      });
    }
    return expArray;
  }

  function handleExperiment(experiment) {
    if (experiment.experimentStatus == "RUNNING") {
      return experiment;
    }
    return false;
  }
};
