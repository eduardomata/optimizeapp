import React from "react";
import SetProperties from "./ExpProperties";

function ExpObjects({ accounts }) {
  return (
    <div className="text-block">
      <div className="text-block-inn text-block-inn__align-left-desktop">
        {accounts.map((account) => (
          <div className="account-tests-wrapper">
            {account.PWRE.map((PWREs) => (
              <SetProperties PWRE={PWREs} />
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default ExpObjects;
