import React from "react";
import SetVariants from "./Variants";

function SetExperiments({ EXPS }) {
  var listClassName = function () {
    if (EXPS.experimentTSessions > 1200) {
      return "test-list stop-it no-bullets pl0";
    }
    return "test-list no-bullets pl0";
  };
  return (
    <ul className={listClassName()}>
      <li className="test-item">
        <ul className="test-item--mdata-holder no-bullets pl0">
          <li>
            <div className="test-item--data-row mdata-row">
              <p className="test-item--name">{EXPS.experimentName}</p>
              <p className="data-row--var-name">
                Total Sessions:
                <span class="regular-text">
                  &nbsp;{EXPS.experimentTSessions}
                </span>
              </p>
              <p className="data-row--var-name">
                Started Date:
                <span class="regular-text">
                  &nbsp;
                  {EXPS.experimentCreationDate.match(
                    /[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]/
                  )}
                </span>
              </p>
            </div>
          </li>
        </ul>
        <SetVariants VAR={EXPS.experimentVariants} />
      </li>
    </ul>
  );
}

export default SetExperiments;
