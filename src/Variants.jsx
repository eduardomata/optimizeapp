import React from "react";

function SetVariants({ VAR }) {
  function toggleIt(e) {
    e.target.classList.toggle("active");
    var panel = e.target.nextElementSibling;
    if (panel.style.height === "100%") {
      panel.style.height = "0";
    } else {
      panel.style.height = "100%";
    }
  }
  return (
    <ul className="test-item--results-holder no-bullets pl0">
      <li>
        <div className="test-item--data-row data-row-1">
          <p className="data-row--var-name text-center">Sessions</p>
          <p className="data-row--var-name text-center">Leads</p>
          <p className="data-row--var-name text-center">Add to cart</p>
          <p className="data-row--var-name text-center">Reservations</p>
        </div>
      </li>
      {VAR.map((variant, index) => (
        <li>
          <div className="test-item--data-row data-row-2">
            <p className="data-row--var-name text-left">
              {variant.name.length
                ? variant.name
                : index === 0
                ? "Original"
                : `Variant ${index}`}
            </p>
            {"result" in variant ? (
              variant.result.map((res, index) =>
                index === 0 ? (
                  <p className="text-center">{res}</p>
                ) : (
                  <p className="text-center">
                    {`${Number.parseFloat(res).toFixed(2)}%`}
                  </p>
                )
              )
            ) : (
              <p>No Results</p>
            )}
          </div>
        </li>
      ))}
      <li>
        <div class="accordion angle-wrapper " onClick={toggleIt}></div>
        <ul class="other-data no-bullets pl0">
          <li className="other-data-row">
            <div className="other-data-names names-l ">
              <p className="data-row--var-name other-data-top-name text-center mb0">
                Leads
              </p>
              <p className="data-row--var-name other-data-sub-name-1 text-center mt0">
                Conversions
              </p>
              <p className="data-row--var-name other-data-sub-name-2 text-center mt0">
                Increase
              </p>
            </div>
            <div className="other-data-names names-r">
              <p className="data-row--var-name other-data-top-name text-center mb0">
                Reservations
              </p>
              <p className="data-row--var-name other-data-sub-name-1 text-center mt0">
                Conversions
              </p>
              <p className="data-row--var-name other-data-sub-name-2 text-center mt0">
                Increase
              </p>
            </div>
          </li>
          {VAR.map((variant, index) => (
            <li className="other-data-row">
              <p className="data-row--var-name names-n  text-left">
                {variant.name.length
                  ? variant.name
                  : index === 0
                  ? "Original"
                  : `Variant ${index}`}
              </p>
              {"result" in variant ? (
                <div className="other-data-names names-l ">
                  <p className="other-data-sub-name-1 text-center">
                    {variant.leadConversions}
                  </p>
                  <p
                    className={`other-data-sub-name-2 text-center ${
                      index === 0 ? "light-gray-text" : ""
                    }`}
                  >
                    {`${Number.parseFloat(variant.leadsIncrease).toFixed(2)}%`}
                  </p>
                </div>
              ) : (
                <p className="names-l text-center">No Results</p>
              )}
              {"result" in variant ? (
                <div className="other-data-names names-r ">
                  <p className="other-data-sub-name-1 text-center">
                    {variant.reservationConversions}
                  </p>
                  <p
                    className={`other-data-sub-name-2 text-center ${
                      index === 0 ? "light-gray-text" : ""
                    }`}
                  >
                    {`${Number.parseFloat(variant.reservationIncrease).toFixed(
                      2
                    )}%`}
                  </p>
                </div>
              ) : (
                <p className="names-r">No Results</p>
              )}
            </li>
          ))}
        </ul>
      </li>
    </ul>
  );
}

export default SetVariants;
