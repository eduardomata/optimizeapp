import React from "react";
import ReactDOM from "react-dom";
import "./style.css";
import NavBar from "./NavBar";
import Start from "./Start";
import Results from "./Results";
import { Router, navigate } from "@reach/router";
import socketIOClient from "socket.io-client";
import Search from "./Search";

const myEndpoit = "/";

const socket = socketIOClient(myEndpoit);
waitHere();
async function waitHere() {
  await new Promise((resolve, err) => {
    setTimeout(function () {
      resolve("Done!");
    }, 200);
  });
  var roomId = window.localStorage.getItem("room-ID");
  socket.emit("room-id", { opt: "join", roomID: roomId });
}

socket.on("goto", (data) => {
  navigate(data);
});

ReactDOM.render(
  <div className="full-content-wrap">
    <NavBar />
    <Router className="router">
      <Start path="/" socket={socket} />
      <Results path="/exp-results" socket={socket} />
      <Search path="/search" socket={socket} />
    </Router>
  </div>,
  document.getElementById("root")
);
