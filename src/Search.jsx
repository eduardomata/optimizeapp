import React from "react";

function Search() {
  function searchSend() {
    var option = document.getElementById("search-opt");
    var searchbox = document.getElementById("searchbox");
    console.log(option, searchbox);
  }
  return (
    <div>
      <label for="search-opt">Search By:</label>

      <select id="search-opt">
        <option value="ProperyName">Property Name</option>
        <option value="ExperimentName">Experiment Name</option>
      </select>
      <input id="searchbox" type="text" placeholder="Search" disabled />
      <input type="submit" value="Search" onclick={searchSend} />
    </div>
  );
}

export default Search;
