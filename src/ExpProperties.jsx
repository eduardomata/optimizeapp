import React from "react";
import SetExperiments from "./Exps";

function SetProperties({ PWRE }) {
  return (
    <div>
      <h3 className="property-name pl1r mt2r">
        <strong>{PWRE.PWREName}</strong>
      </h3>
      {PWRE.PWREExps.map((EXP) => (
        <SetExperiments EXPS={EXP} />
      ))}
    </div>
  );
}

export default SetProperties;
