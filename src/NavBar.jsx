import React from "react";
import { Link } from "@reach/router";

function NavBar() {
  return (
    <header className="nav-bar">
      <div class="items-wrap">
        <div className="nav-logo-wrap">
          <Link to="/exp-results">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="264.143"
              height="27.011"
              viewBox="0 0 264.143 27.011"
            >
              <g
                id="Group_2"
                data-name="Group 2"
                transform="translate(-34.661 90.122)"
              >
                <path
                  id="Path_4"
                  data-name="Path 4"
                  d="M82.473-69.026l-1.622.516-.191-2.438,1.639-2.44c-.018-.314-.029-.613-.029-.888A17.563,17.563,0,0,1,84.05-81.9h0a17.558,17.558,0,0,1,1.782,7.623c0,.274-.011.569-.029.883l1.643,2.445-.19,2.438-1.623-.516-.35,5.325h12.4L84.054-90.122,70.421-63.7h12.4Z"
                  transform="translate(-14.664 0)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_5"
                  data-name="Path 5"
                  d="M39.488-88.728v21.14H52.657v4.464h-18v-25.6Z"
                  transform="translate(0 -0.572)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_6"
                  data-name="Path 6"
                  d="M140.239-88.668V-74c0,7.571-4.644,11.45-11.083,11.45-6.145,0-11.378-3.731-11.378-11.45V-88.668h4.794V-74c0,4.609,2.634,7.058,6.62,7.058s6.256-2.669,6.256-7.058V-88.668Z"
                  transform="translate(-34.084 -0.596)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_7"
                  data-name="Path 7"
                  d="M179.812-88.791h4.827v25.642h-3v.034l-13.459-17.3v17.265h-4.829v-25.6h3.912L179.812-72.88Z"
                  transform="translate(-52.773 -0.546)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_8"
                  data-name="Path 8"
                  d="M229-66.7a13.5,13.5,0,0,1-9.728,3.8c-9.512,0-13.535-6.548-13.573-13.206-.035-6.693,4.318-13.5,13.573-13.5a13.212,13.212,0,0,1,9.4,3.915l-3.22,3.108a8.8,8.8,0,0,0-6.18-2.415c-6.184,0-8.853,4.609-8.817,8.888.035,4.244,2.488,8.67,8.817,8.67a9.434,9.434,0,0,0,6.437-2.6Z"
                  transform="translate(-70.139 -0.214)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_9"
                  data-name="Path 9"
                  d="M268.108-63.065V-73.414H255.523v10.349h-4.83v-25.6h4.83v10.791h12.585V-88.668H272.9v25.6Z"
                  transform="translate(-88.59 -0.596)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_10"
                  data-name="Path 10"
                  d="M316.742-81.3a5.208,5.208,0,0,1-3.331,4.938,6.238,6.238,0,0,1,4.1,5.705c0,5.706-4.28,7.536-9.582,7.536H295.783v-25.6h12.144C312.975-88.728,316.742-86.462,316.742-81.3ZM300.536-78.45h7.391c3.035,0,4.023-1.281,4.023-2.743,0-1.172-.952-3.146-4.023-3.146h-7.391Zm0,10.827h7.391c1.974,0,4.79-.658,4.79-3.071,0-2.307-2.816-3.513-4.79-3.513h-7.391Z"
                  transform="translate(-107.08 -0.572)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_11"
                  data-name="Path 11"
                  d="M363.438-76.138c-.073,6.62-4.132,13.242-13.132,13.242S337.138-69.371,337.138-76.1s4.316-13.5,13.169-13.5C359.121-89.6,363.511-82.831,363.438-76.138Zm-21.545.11c.109,4.206,2.377,8.7,8.413,8.7s8.3-4.534,8.375-8.74c.076-4.317-2.34-9.146-8.375-9.146S341.782-80.344,341.894-76.028Z"
                  transform="translate(-124.039 -0.214)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_12"
                  data-name="Path 12"
                  d="M412.116-76.138c-.073,6.62-4.135,13.242-13.133,13.242S385.816-69.371,385.816-76.1s4.317-13.5,13.168-13.5C407.8-89.6,412.189-82.831,412.116-76.138Zm-21.547.11c.112,4.206,2.38,8.7,8.415,8.7s8.3-4.534,8.376-8.74c.073-4.317-2.343-9.146-8.376-9.146S390.46-80.344,390.569-76.028Z"
                  transform="translate(-144.001 -0.214)"
                  fill="#fbbf43"
                ></path>
                <path
                  id="Path_13"
                  data-name="Path 13"
                  d="M458.752-81.645l-8.34,11.12h-.952l-8.154-11.157v18.617h-4.828v-25.6h5.559l8.011,11.009,8.01-11.009h5.523v25.6h-4.829Z"
                  transform="translate(-164.776 -0.596)"
                  fill="#fbbf43"
                ></path>
              </g>
            </svg>
          </Link>
        </div>
        <div class="nav-items">
          <Link className="nav-item" to="/exp-results">
            Running Exps
          </Link>
          <Link className="nav-item" to="/search">
            Search
          </Link>
          <a className="nav-item" href="/">
            Refresh
          </a>
        </div>
      </div>
    </header>
  );
}
export default NavBar;
