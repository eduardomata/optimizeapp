import React, { useState, useEffect } from "react";

function Start({ socket }) {
  const [messages, setMessages] = useState(["Waiting for a response..."]);
  useEffect(
    function onMessage() {
      socket.on("initmessage", (data) => {
        setMessages(data);
      });
    },
    [socket, messages, setMessages]
  );
  return (
    <div className="sct-wrap start-sct-wrap">
      <div className="text-block">
        <div className="text-block-inn">
          <div className="loading-icon">
            <svg
              version="1.1"
              viewBox="-58 -58 116 116"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g stroke-linecap="round" stroke-width="15">
                <path id="a" d="m0 35 0,14" />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(210)"
                  stroke="#f0f0f0"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(240)"
                  stroke="#ebebeb"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(270)"
                  stroke="#d3d3d3"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(300)"
                  stroke="#bcbcbc"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(330)"
                  stroke="#a4a4a4"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(0)"
                  stroke="#8d8d8d"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(30)"
                  stroke="#757575"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(60)"
                  stroke="#5e5e5e"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(90)"
                  stroke="#464646"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(120)"
                  stroke="#2f2f2f"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(150)"
                  stroke="#2f2f2f"
                />
                <path
                  id="a"
                  d="m0 35 0,14"
                  transform="rotate(180)"
                  stroke="#2f2f2f"
                />
              </g>
            </svg>
          </div>
          <code className="initial-messages">{messages}</code>
        </div>
      </div>
    </div>
  );
}

export default Start;
