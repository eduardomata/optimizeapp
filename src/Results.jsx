import React, { useState, useEffect } from "react";
import ExpObjects from "./ExpObjects";
function App({ socket }) {
  const [accounts, setAccounts] = useState([]);

  useEffect(() => {
    socket.on("results", (data) => {
      console.log(data);
      setAccounts(data);
    });
  }, [socket, setAccounts]);

  return (
    <div className="sct-wrap">
      <div className="text-block">
        <div className="text-block-inn text-block-inn__align-left-desktop">
          <h1>Running Experiments</h1>
        </div>
      </div>
      <ExpObjects accounts={accounts} />
    </div>
  );
}

export default App;
