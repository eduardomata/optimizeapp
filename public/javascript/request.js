/**
 * This Will request Account, property
 * and experiments info to put it into
 * the ACCOUNTS array
 * **/
var CLIENT_ID =
  "305822761178-o9481a1h46d27qoe2itppa98942fc80o.apps.googleusercontent.com";
var SCOPES = ["https://www.googleapis.com/auth/analytics.readonly"];

var ACCOUNTS = [];

class newAccount {
  constructor(accountId, accountName, accountProperties = []) {
    this.accountId = accountId;
    this.accountName = accountName;
    this.accountProperties = accountProperties;
  }
}

class newProperty {
  constructor(
    propertyName,
    propertyId,
    propertyProfileId,
    propertyExperiments = []
  ) {
    this.propertyName = propertyName;
    this.propertyId = propertyId;
    this.propertyProfileId = propertyProfileId;
    this.propertyExperiments = propertyExperiments;
  }
}

class newExperiment {
  constructor(
    experimentName,
    experimentId,
    experimentStatus,
    experimentCreationDate,
    experimentEndDate,
    experimentVariants = []
  ) {
    this.experimentName = experimentName;
    this.experimentId = experimentId;
    this.experimentStatus = experimentStatus;
    this.experimentCreationDate = experimentCreationDate;
    this.experimentEndDate = experimentEndDate;
    this.experimentVariants = experimentVariants;
  }
}

function authorize(event) {
  console.log("HERE??");
  var useImmdiate = event ? false : true;
  var authData = {
    client_id: CLIENT_ID,
    scope: SCOPES,
    immediate: useImmdiate,
  };
  emmit("startmessages", "Waiting for Authorization...");

  gapi.auth.authorize(authData, function (response) {
    var authButton = document.getElementById("auth-button");
    if (response.error) {
      emmit("startmessages", "Still need to verify your Google account");
      authButton.hidden = false;
    } else {
      authButton.hidden = true;
      queryAccounts();
    }
  });
}

function queryAccounts() {
  emmit("startmessages", "Getting Google accounts info...");

  gapi.client.load("analytics", "v3").then(function () {
    gapi.client.analytics.management.accounts.list().then(handleAccounts);
  });
}

async function handleAccounts(response) {
  var accountsCounter = 0;

  if (response.result.items && response.result.items.length) {
    for (var i = 0; i < response.result.items.length; i++) {
      if (
        response.result.items[i].name.includes("LaunchBoom Customer Websites")
      ) {
        accountsCounter++;
        ACCOUNTS.push(
          new newAccount(
            response.result.items[i].id,
            response.result.items[i].name
          )
        );

        await getAccountData(response.result.items[i].id, accountsCounter - 1);
      }
    }

    emmit("startmessages", "We're almost done...");
    var wait = await new Promise((resolve, err) => {
      setTimeout(function () {
        resolve("Done!");
      }, 2000);
    });
    console.log(wait);
    emmit("send-accounts", ACCOUNTS);
  } else {
    console.log("No accounts found for this user.");
  }
}

function getAccountData(accountId, accountsCounter) {
  var propertiesCounter;

  return queryProperties();

  function queryProperties() {
    emmit("startmessages", "Getting properties info...");

    return gapi.client.analytics.management.webproperties
      .list({ accountId: accountId })
      .then(handleProperties)
      .then(null, function (err) {
        console.log(err);
      });
  }

  async function handleProperties(response) {
    if (response.result.items && response.result.items.length) {
      emmit("startmessages", "Getting experiments info...");

      for (
        propertiesCounter = 0;
        propertiesCounter < response.result.items.length;
        propertiesCounter++
      ) {
        var AccountId = response.result.items[propertiesCounter].accountId;
        var PropertyName = response.result.items[propertiesCounter].name;
        var PropertyId = response.result.items[propertiesCounter].id;
        var ProfileId =
          response.result.items[propertiesCounter].defaultProfileId;
        ACCOUNTS[accountsCounter].accountProperties.push(
          new newProperty(PropertyName, PropertyId, ProfileId)
        );

        await getExperiments(
          AccountId,
          PropertyId,
          ProfileId,
          accountsCounter,
          propertiesCounter
        );
      }
    } else {
      console.log("No properties found for this user.");
    }
  }
  function getExperiments(
    AccountId,
    PropertyId,
    ProfileId,
    accountsCounter,
    propertiesCounter
  ) {
    return queryExperiments();
    function queryExperiments() {
      return gapi.client.analytics.management.experiments
        .list({
          accountId: AccountId,
          webPropertyId: PropertyId,
          profileId: ProfileId,
        })
        .execute((resp) => {
          handleExperiments(resp);
        });
    }

    async function handleExperiments(results) {
      if (results && !results.error) {
        var experiments = results.items;
        for (var i = 0; i < experiments.length; i++) {
          ACCOUNTS[accountsCounter].accountProperties[
            propertiesCounter
          ].propertyExperiments.push(
            new newExperiment(
              experiments[i].name,
              experiments[i].id,
              experiments[i].status,
              experiments[i].startTime,
              experiments[i].endTime,
              experiments[i].variations
            )
          );
        }
        await getReportingData(
          ProfileId,
          ACCOUNTS[accountsCounter].accountProperties[propertiesCounter]
            .propertyExperiments
        );
      }
    }
    function getReportingData(ProfileId, experiments) {
      return queryExperimentReports();

      function queryExperimentReports() {
        return gapi.client
          .request({
            path: "/v4/reports:batchGet",
            root: "https://analyticsreporting.googleapis.com/",
            method: "POST",
            body: {
              reportRequests: [
                {
                  viewId: ProfileId,
                  dateRanges: [
                    {
                      startDate: "2005-01-01",
                      endDate: "today",
                    },
                  ],
                  metrics: [
                    {
                      expression: "ga:sessions",
                    },
                    {
                      expression: "ga:goal1ConversionRate",
                    },
                    {
                      expression: "ga:goal2ConversionRate",
                    },
                    {
                      expression: "ga:goal3ConversionRate",
                    },
                  ],
                  dimensions: [
                    {
                      name: "ga:experimentId",
                    },
                    {
                      name: "ga:experimentVariant",
                    },
                  ],
                },
              ],
            },
          })
          .then(handleReports, console.error.bind(console));
      }

      function handleReports(response) {
        if (response.result.reports[0].data.rows && !response.error) {
          response.result.reports[0].data.rows.forEach((variant) => {
            experiments.forEach((exp) => {
              if (variant.dimensions[0] == exp.experimentId) {
                var position = variant.dimensions[1];
                exp.experimentVariants[position].result =
                  variant.metrics[0].values;
              }
            });
          });
        }
      }
    }
  }
}

document.getElementById("auth-button").addEventListener("click", authorize);
