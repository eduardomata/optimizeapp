var socket = io.connect("/", { transports: ["websocket"] });

emmit("room-id", "create");
socket.on("room-ID", (data) => {
  window.localStorage.setItem("room-ID", data);
});

emmit("startmessages", "Websocket connected");

function emmit(api, content) {
  socket.emit(api, content);
}
